#ifndef IPokerTypeEval_H
#define IPokerTypeEval_H

#include <vector>
using std::vector;

#include "PokerEvalDef.h"

class IPokerTypeEval 
{
public:
    virtual ~IPokerTypeEval(){};
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards) = 0;

};

#endif