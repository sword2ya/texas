#ifndef PokerEvalDef_H
#define PokerEvalDef_H

#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <cassert>
using namespace std;



enum ECardPoint 
{
    eCardPoint_2,
    eCardPoint_3,
    eCardPoint_4,
    eCardPoint_5,
    eCardPoint_6,
    eCardPoint_7,
    eCardPoint_8,
    eCardPoint_9,
    eCardPoint_10,
    eCardPoint_J,
    eCardPoint_Q,
    eCardPoint_K,
    eCardPoint_A,
};

enum ECardColor 
{
    eCardColor_Black,
    eCardColor_Red,
    eCardColor_Club,
    eCardColor_Diamond,
};

struct TPokerCard 
{
    int color; // 0123 => 黑红梅方， 参考 ECardColor
    int point; // 0-12 => 2,3,4,5,..,J,Q,K,A， 参考ECardPoint

    TPokerCard(int c, int p) : color(c), point(p)
    {
    }

    bool operator==(const TPokerCard& other) const 
    {
        return color == other.color && point == other.point;
    }

    bool operator<(const TPokerCard& other) const 
    {
        if (point != other.point) {
            return point < other.point;
        }
        return color < other.color;
    }

    bool operator>(const TPokerCard& other) const 
    {
        return !(*this < other) && !(*this == other);
    }
};

enum EPokerType 
{
    ePokerType_HighCard,    // 高牌
    ePokerType_Pair,        // 对子
    ePokerType_TwoPair,     // 两对
    ePokerType_Three,       // 三条
    ePokerType_Straight,    // 顺子
    ePokerType_Flush,       // 同花
    ePokerType_FullHouse,   // 葫芦
    ePokerType_Four,        // 四条
    ePokerType_StraightFlush, // 同花顺
    ePokerType_Royal,       // 皇家同花顺
};

struct TPokerValue 
{
    int poker_type; // 参考 EPokerType
    int type_value; // 牌型评估大小
};


#endif