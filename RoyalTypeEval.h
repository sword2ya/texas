#ifndef _RoyalTypeEval_H
#define _RoyalTypeEval_H 

#include "IPokerTypeEval.h"

class CRoyalTypeEval : public IPokerTypeEval 
{
public:
    virtual ~CRoyalTypeEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);
};

#endif