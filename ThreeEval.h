#ifndef _ThreeEval_H
#define _ThreeEval_H

#include "IPokerTypeEval.h"

class CThreeEval : public IPokerTypeEval
{
public:
    ~CThreeEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);
};


#endif