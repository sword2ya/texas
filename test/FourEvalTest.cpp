#include "gtest/gtest.h"
#include "FourEval.h"

class CFourEvalTest : public testing::Test 
{
public:
    void assertFour(int point, const vector<TPokerCard>& cards) 
    {
        ASSERT_GE(cards.size(), 4);
        for (int i = 0; i < 4; ++i) {
            ASSERT_EQ(cards[i].point, point);
        }
    }

protected:
    CFourEval _four_eval;
};

// 2,2,2,2,A
TEST_F(CFourEvalTest, Is_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_four_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    assertFour(eCardPoint_2, match_cards);
    ASSERT_EQ(eCardPoint_A, match_cards[4].point);
}

// 2,2,2,2,5,6 => 2,2,2,2,6
TEST_F(CFourEvalTest, Is_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_four_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    assertFour(eCardPoint_2, match_cards);
    ASSERT_EQ(eCardPoint_6, match_cards[4].point);
}

// 2,2,2,2
TEST_F(CFourEvalTest, Is_2) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_2));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_four_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 4);
    assertFour(eCardPoint_2, match_cards);
}

// 2,2,2,A,K
TEST_F(CFourEvalTest, Not_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_four_eval.isMatch(cards, match_cards));
}

// 2,2,A,K,Q
TEST_F(CFourEvalTest, Not_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_Q));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_four_eval.isMatch(cards, match_cards));
}

