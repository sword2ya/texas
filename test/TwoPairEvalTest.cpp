#include "gtest/gtest.h"
#include "TwoPairEval.h"

class CTwoPairEvalTest : public testing::Test 
{
public:
    void assertMatchCard(int point_1, int point_2, const vector<TPokerCard>& match_cards) 
    {  
        ASSERT_GE(match_cards.size(), 4);

        for (int i = 0; i < 2; ++i) {
            ASSERT_EQ(match_cards[i].point, point_1);
        }
        for (int i = 3; i < 4; ++i) {
            ASSERT_EQ(match_cards[i].point, point_2);
        }
    }

protected:
    CTwoPairEval _twopair_eval;
};

// 2,2,3,3,A => 3,3,2,2,A
TEST_F(CTwoPairEvalTest, Is_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_twopair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    assertMatchCard(eCardPoint_3, eCardPoint_2, match_cards);
    ASSERT_EQ(match_cards[4], TPokerCard(eCardColor_Club, eCardPoint_A));
}

// 2,2,3,3,A,A => A,A,3,3,2
TEST_F(CTwoPairEvalTest, Is_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_twopair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    assertMatchCard(eCardPoint_A, eCardPoint_3, match_cards);
    ASSERT_EQ(match_cards[4].point, eCardPoint_2);
}

// 2,2,3,3 => 3,3,2,2
TEST_F(CTwoPairEvalTest, Is_2) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_twopair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 4);
    assertMatchCard(eCardPoint_3, eCardPoint_2, match_cards);
}

// 2,2,3,A,K
TEST_F(CTwoPairEvalTest, Not_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_twopair_eval.isMatch(cards, match_cards));
}

// 2,3,4,A,K
TEST_F(CTwoPairEvalTest, Not_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_twopair_eval.isMatch(cards, match_cards));
}

