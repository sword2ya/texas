#include "gtest/gtest.h"
#include "FlushEval.h"

class CFlushEvalTest : public testing::Test 
{
public:

protected:
    CFlushEval _flush_eval;
};

// black 2,3,4,5,6
TEST_F(CFlushEvalTest, Is_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));

    vector<TPokerCard> expected_match_cards;
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_flush_eval.isMatch(cards, match_cards));
    ASSERT_EQ(expected_match_cards, match_cards);
}

// black 2,3,4,5,K,A
TEST_F(CFlushEvalTest, Is_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));

    vector<TPokerCard> expected_match_cards;
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_flush_eval.isMatch(cards, match_cards));
    ASSERT_EQ(expected_match_cards, match_cards);
}

// black 2,3,4,5
// red A
TEST_F(CFlushEvalTest, Not_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_flush_eval.isMatch(cards, match_cards));
}

// black 2,3,4
TEST_F(CFlushEvalTest, Not_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_flush_eval.isMatch(cards, match_cards));
}


// black 2,3,4
// red 5,6
TEST_F(CFlushEvalTest, Not_2) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_6));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_flush_eval.isMatch(cards, match_cards));
}
