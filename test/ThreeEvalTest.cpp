#include "gtest/gtest.h"
#include "ThreeEval.h"


class CThreeEvalTest : public testing::Test 
{

public:
    void assertThree(int point, const vector<TPokerCard>& cards) 
    {
        for (int i = 0; i < 3; ++i) {
            ASSERT_EQ(cards[i].point, point);
        }
    }
protected:
    CThreeEval _three_eval;
};

// 2,2,2,A,K
TEST_F(CThreeEvalTest, Is_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_three_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    assertThree(eCardPoint_2, match_cards);
    ASSERT_EQ(eCardPoint_A, match_cards[3].point);
    ASSERT_EQ(eCardPoint_K, match_cards[4].point);
}

// 2,2,2,A
TEST_F(CThreeEvalTest, Is_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_three_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 4);
    assertThree(eCardPoint_2, match_cards);
    ASSERT_EQ(eCardPoint_A, match_cards[3].point);
}

// 2,2,2
TEST_F(CThreeEvalTest, Is_2) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_three_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 3);
    assertThree(eCardPoint_2, match_cards);
}

// 2,2,3,3,3,A,K
TEST_F(CThreeEvalTest, Is_3) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_three_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    assertThree(eCardPoint_3, match_cards);
    ASSERT_EQ(eCardPoint_A, match_cards[3].point);
    ASSERT_EQ(eCardPoint_K, match_cards[4].point);
}

// 3,3,3,A,A,K,K
TEST_F(CThreeEvalTest, Is_4) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_K));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_three_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    assertThree(eCardPoint_3, match_cards);
    ASSERT_EQ(eCardPoint_A, match_cards[3].point);
    ASSERT_EQ(eCardPoint_A, match_cards[4].point);
}

// 2,2,A,K
TEST_F(CThreeEvalTest, Not_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_three_eval.isMatch(cards, match_cards));
}

// 2,A,K,Q
TEST_F(CThreeEvalTest, Not_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_Q));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_three_eval.isMatch(cards, match_cards));
}

