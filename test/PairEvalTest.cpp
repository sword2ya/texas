#include "gtest/gtest.h"
#include "PairEval.h"


class CPairEvalTest : public testing::Test 
{

public:
    void assertPair(int point, const vector<TPokerCard>& cards) 
    {
        for (int i = 0; i < 2; ++i) {
            ASSERT_EQ(cards[i].point, point);
        }
    }
protected:
    CPairEval _pair_eval;
};

// 2,2,A,K,Q
TEST_F(CPairEvalTest, Is_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_Q));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    assertPair(eCardPoint_2, match_cards);
    ASSERT_EQ(eCardPoint_A, match_cards[2].point);
    ASSERT_EQ(eCardPoint_K, match_cards[3].point);
    ASSERT_EQ(eCardPoint_Q, match_cards[4].point);
}

// 2,2,A,K
TEST_F(CPairEvalTest, Is_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_K));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 4);
    assertPair(eCardPoint_2, match_cards);
    ASSERT_EQ(eCardPoint_A, match_cards[2].point);
    ASSERT_EQ(eCardPoint_K, match_cards[3].point);
}

// 2,2
TEST_F(CPairEvalTest, Is_2) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 2);
    assertPair(eCardPoint_2, match_cards);
}

// 2,2,3,3,A => 3,3,A,2,2
TEST_F(CPairEvalTest, Is_3) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    assertPair(eCardPoint_3, match_cards);
    ASSERT_EQ(eCardPoint_A, match_cards[2].point);
    ASSERT_EQ(eCardPoint_2, match_cards[3].point);
    ASSERT_EQ(eCardPoint_2, match_cards[4].point);
}

// 2,3,4,5,6
TEST_F(CPairEvalTest, Not_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_pair_eval.isMatch(cards, match_cards));
}

// 2,A,K,Q
TEST_F(CPairEvalTest, Not_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_Q));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_pair_eval.isMatch(cards, match_cards));
}

