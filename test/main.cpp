#include "gtest/gtest.h"

using namespace testing;

int main(int argc, char* argv[])
{
    InitGoogleTest(&argc, argv);
    int nResult = RUN_ALL_TESTS();
    return nResult;
}