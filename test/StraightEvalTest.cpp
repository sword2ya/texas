#include "gtest/gtest.h"
#include "StraightEval.h"

class CStraightEvalTest : public testing::Test 
{
public:

protected:
    CStraightEval _straight_eval;
};

// 2,3,4,5,6
TEST_F(CStraightEvalTest, Is_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));

    vector<TPokerCard> expected_match_cards;
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_straight_eval.isMatch(cards, match_cards));
    ASSERT_EQ(expected_match_cards, match_cards);
}

// A,2,3,4,5
TEST_F(CStraightEvalTest, Is_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));

    vector<TPokerCard> expected_match_cards;
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_straight_eval.isMatch(cards, match_cards));
    ASSERT_EQ(expected_match_cards, match_cards);
}

// 2,3,4,5,6,7 => 3,4,5,6,7
TEST_F(CStraightEvalTest, Is_2) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_7));

    vector<TPokerCard> expected_match_cards;
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_7));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_straight_eval.isMatch(cards, match_cards));
    ASSERT_EQ(expected_match_cards, match_cards);    
}


// A,2,3,4,5,6 => 2,3,4,5,6
TEST_F(CStraightEvalTest, Is_3) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));

    vector<TPokerCard> expected_match_cards;
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_6));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_straight_eval.isMatch(cards, match_cards));
    ASSERT_EQ(expected_match_cards, match_cards);    
}

// 2,3,4,5,7
TEST_F(CStraightEvalTest, Not_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_7));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_straight_eval.isMatch(cards, match_cards));
}

// K,A,2,3,4
TEST_F(CStraightEvalTest, Not_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_4));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_straight_eval.isMatch(cards, match_cards));
}
