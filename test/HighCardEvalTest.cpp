#include "gtest/gtest.h"
#include "HighCardEval.h"


class CHighCardEvalTest : public testing::Test 
{
protected:
    CHighCardEval _pair_eval;
};

// 2,3,4,5,6
TEST_F(CHighCardEvalTest, Is_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_6));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    ASSERT_EQ(eCardPoint_6, match_cards[0].point);
    ASSERT_EQ(eCardPoint_5, match_cards[1].point);
    ASSERT_EQ(eCardPoint_4, match_cards[2].point);
    ASSERT_EQ(eCardPoint_3, match_cards[3].point);
    ASSERT_EQ(eCardPoint_2, match_cards[4].point);
}

// 6,5,4,3,2
TEST_F(CHighCardEvalTest, Is_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_6));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    ASSERT_EQ(eCardPoint_6, match_cards[0].point);
    ASSERT_EQ(eCardPoint_5, match_cards[1].point);
    ASSERT_EQ(eCardPoint_4, match_cards[2].point);
    ASSERT_EQ(eCardPoint_3, match_cards[3].point);
    ASSERT_EQ(eCardPoint_2, match_cards[4].point);
}

// 2,3,4,5,6,7
TEST_F(CHighCardEvalTest, Is_2) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_6));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_7));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    ASSERT_EQ(eCardPoint_7, match_cards[0].point);
    ASSERT_EQ(eCardPoint_6, match_cards[1].point);
    ASSERT_EQ(eCardPoint_5, match_cards[2].point);
    ASSERT_EQ(eCardPoint_4, match_cards[3].point);
    ASSERT_EQ(eCardPoint_3, match_cards[4].point);
}

// 2,3,4,5,A
TEST_F(CHighCardEvalTest, Is_3) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_5));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 5);
    ASSERT_EQ(eCardPoint_A, match_cards[0].point);
    ASSERT_EQ(eCardPoint_5, match_cards[1].point);
    ASSERT_EQ(eCardPoint_4, match_cards[2].point);
    ASSERT_EQ(eCardPoint_3, match_cards[3].point);
    ASSERT_EQ(eCardPoint_2, match_cards[4].point);
}

// 2,3,4,A
TEST_F(CHighCardEvalTest, Is_4) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_4));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 4);
    ASSERT_EQ(eCardPoint_A, match_cards[0].point);
    ASSERT_EQ(eCardPoint_4, match_cards[1].point);
    ASSERT_EQ(eCardPoint_3, match_cards[2].point);
    ASSERT_EQ(eCardPoint_2, match_cards[3].point);
}

// 2
TEST_F(CHighCardEvalTest, Is_5) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_pair_eval.isMatch(cards, match_cards));
    ASSERT_EQ(match_cards.size(), 1);
    ASSERT_EQ(eCardPoint_2, match_cards[0].point);
}
