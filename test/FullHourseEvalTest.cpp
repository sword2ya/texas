#include "gtest/gtest.h"
#include "FullHourseEval.h"

class CFullHourseEvalTest : public testing::Test 
{
public:
    void assertMatchCard(int point_1, int point_2, const vector<TPokerCard>& match_cards) 
    {  
        ASSERT_EQ(match_cards.size(), 5);

        for (int i = 0; i < 3; ++i) {
            ASSERT_EQ(match_cards[i].point, point_1);
        }
        for (int i = 3; i < 5; ++i) {
            ASSERT_EQ(match_cards[i].point, point_2);
        }
    }

protected:
    CFullHourseEval _fullhourse_eval;
};

// 2,2,2,3,3
TEST_F(CFullHourseEvalTest, Is_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_fullhourse_eval.isMatch(cards, match_cards));
    assertMatchCard(eCardPoint_2, eCardPoint_3, match_cards);
}

// 2,2,3,3,3
TEST_F(CFullHourseEvalTest, Is_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_3));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_fullhourse_eval.isMatch(cards, match_cards));
    assertMatchCard(eCardPoint_3, eCardPoint_2, match_cards);
}

// 2,2,2,3,3,3 => 3,3,3,2,2
TEST_F(CFullHourseEvalTest, Is_2) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_3));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_fullhourse_eval.isMatch(cards, match_cards));
    assertMatchCard(eCardPoint_3, eCardPoint_2, match_cards);
}

// 2,2,2,2,3,3,3 => 3,3,3,2,2
TEST_F(CFullHourseEvalTest, Is_3) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Diamond, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_3));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_fullhourse_eval.isMatch(cards, match_cards));
    assertMatchCard(eCardPoint_3, eCardPoint_2, match_cards);
}

// 2,2,2,A,K
TEST_F(CFullHourseEvalTest, Not_0) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Club, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_fullhourse_eval.isMatch(cards, match_cards));
}

// 2,2,3,3,A
TEST_F(CFullHourseEvalTest, Not_1) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_3));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_fullhourse_eval.isMatch(cards, match_cards));
}

