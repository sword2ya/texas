#include "gtest/gtest.h"
#include "RoyalTypeEval.h"

class CRoyalTypeEvalTest : public testing::Test 
{
public:

protected:
    CRoyalTypeEval _royal_type_eval;
};

TEST_F(CRoyalTypeEvalTest, Test_IsRoyal5)
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_10));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_J));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_Q));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));

    vector<TPokerCard> expected_match_cards;
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_Q));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_J));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_10));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_royal_type_eval.isMatch(cards, match_cards));
    ASSERT_EQ(expected_match_cards, match_cards);
}

TEST_F(CRoyalTypeEvalTest, Test_IsRoyal7) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_10));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_J));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_8));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_2));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_Q));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));

    vector<TPokerCard> expected_match_cards;
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_Q));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_J));
    expected_match_cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_10));

    vector<TPokerCard> match_cards;
    ASSERT_TRUE(_royal_type_eval.isMatch(cards, match_cards));
    ASSERT_EQ(expected_match_cards, match_cards);    
}

TEST_F(CRoyalTypeEvalTest, Test_NotRoyal_Point) 
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_9));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_J));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_Q));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_royal_type_eval.isMatch(cards, match_cards));
}

TEST_F(CRoyalTypeEvalTest, Test_NotRoyal_Color)
{
    vector<TPokerCard> cards;
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_10));
    cards.push_back(TPokerCard(eCardColor_Red, eCardPoint_J));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_Q));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_K));
    cards.push_back(TPokerCard(eCardColor_Black, eCardPoint_A));

    vector<TPokerCard> match_cards;
    ASSERT_FALSE(_royal_type_eval.isMatch(cards, match_cards));
}