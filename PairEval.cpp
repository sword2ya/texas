#include "PairEval.h"
#include "CountMatchUtils.h"

CPairEval::~CPairEval()
{
}


bool CPairEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    return CCountMatchUtil::isMatch(cards, 2, match_cards);
}
