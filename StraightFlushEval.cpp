#include "StraightFlushEval.h"
#include "FlushEval.h"
#include "StraightEval.h"

CStraightFlushEval::~CStraightFlushEval()
{
}

bool CStraightFlushEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    static CFlushEval flush_eval;
    static CStraightEval straight_eval;

    vector<TPokerCard> flush_match_cards;
    if (!flush_eval.isMatch(cards, flush_match_cards)) {
        return false;
    }
    assert(!flush_match_cards.empty());
    int color = flush_match_cards[0].color;

    vector<TPokerCard> new_cards;

    for (vector<TPokerCard>::const_iterator iter = cards.begin();
        iter != cards.end();
        ++iter) {
        if (iter->color == color) {
            new_cards.push_back(*iter);
        }
    }

    return straight_eval.isMatch(new_cards, match_cards);
}
