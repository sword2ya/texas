#include "FlushEval.h"

CFlushEval::~CFlushEval()
{
}


bool CFlushEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    // 暂时只支持7张及以下的牌型判断，避免有两个不同花色的同花存在
    if (cards.size() < 5 || cards.size() > 7) { 
        return false;
    }
    typedef set<TPokerCard, std::greater<TPokerCard>> GreaterCardSet;
    typedef map<int, GreaterCardSet > ColorToCardsMap;

    ColorToCardsMap color_to_cards;

    for (vector<TPokerCard>::const_iterator it = cards.begin(); it != cards.end(); ++it) {
        const TPokerCard& card = *it;
        color_to_cards[card.color].insert(card);
    }

    for (ColorToCardsMap::iterator it = color_to_cards.begin(); 
        it != color_to_cards.end(); 
        ++it) {
        GreaterCardSet& cards = it->second;
        if (cards.size() < 5) {
            continue;
        }

        for (GreaterCardSet::iterator card_iter = cards.begin(); card_iter != cards.end(); ++card_iter) {
            match_cards.push_back(*card_iter);
            if (match_cards.size() >= 5) {
                break;
            }
        }
        return true;
    }

    return false;
}
