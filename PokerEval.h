#ifndef _PokerEval_H
#define _PokerEval_H

#include <map>
using std::map;

#include "IPokerTypeEval.h"
#include "PokerEval.h"

class CPokerEval 
{
public:
    CPokerEval();
    ~CPokerEval();
    TPokerValue calcPokerType(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);

private:
    int calcTypeValue(const vector<TPokerCard>& cards);

private:
    typedef map<int, IPokerTypeEval*, std::greater<int> > PokerEvalMap;
    PokerEvalMap _type_evaler_map;
};

#endif