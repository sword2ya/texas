
CC          = gcc
CXX         = g++
CFLAGS      += -g -fPIC -Wno-deprecated -Wall
CFLAGS      += -std=c++11 -lpthread

TARGET = libtexaseval.a

BASE_DIR = ./

INCLUDE += -I$(BASE_DIR)

LOCAL_SRC += $(sort $(wildcard $(BASE_DIR)/*.cpp))

LOCAL_OBJ += $(patsubst %.cpp,%.o, $(patsubst %.c,%.o, $(LOCAL_SRC)))
DEP_FILE    := $(foreach obj, $(LOCAL_OBJ), $(dir $(obj)).$(basename $(notdir $(obj))).d)

$(TARGET) : $(LOCAL_OBJ)
	@echo "src:" $(LOCAL_SRC)
	@echo "obj:" $(LOCAL_OBJ)
	ar r $@ $(LOCAL_OBJ)

.%.d: %.cpp
	@echo "update $@ ..."; \
	echo -n $< | sed s/\.cpp/\.o:/ > $@; \
	$(CC) $(INCLUDE) -MM $< | sed '1s/.*.://' >> $@;

%.o: %.cpp
	$(CXX) -m64 $(CFLAGS) $(INCLUDE) -o $@ -c $<

.%.d: %.c
	@echo "update $@ ..."; \
	echo -n $< | sed s/\.c/\.o:/ > $@; \
	$(CC) $(INCLUDE) -MM $< | sed '1s/.*.://' >> $@;

%.o: %.c
	$(CC) -m64 $(CFLAGS) $(INCLUDE) -o $@ -c $<


clean:
	rm -vf $(LOCAL_OBJ) $(TARGET) .*.d.tmp $(DEP_FILE)



ifneq ($(DEP_FILE),)
-include $(DEP_FILE)
endif