#ifndef _FlushEval_H
#define _FlushEval_H

#include "IPokerTypeEval.h"

class CFlushEval : public IPokerTypeEval 
{
    typedef map<int, set<TPokerCard> > Point2CardsMap;
public:
    ~CFlushEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);
};


#endif