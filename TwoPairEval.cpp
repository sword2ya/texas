#include "TwoPairEval.h"

CTwoPairEval::~CTwoPairEval()
{
}


bool CTwoPairEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    if (cards.size() < 4) { 
        return false;
    }
    typedef map<int, set<TPokerCard>, std::greater<int> > PointToCardMap;
    PointToCardMap point_to_cards;
    typedef set<int, std::greater<int> > PointSet;

    for (vector<TPokerCard>::const_iterator it = cards.begin(); it != cards.end(); ++it) {
        const TPokerCard& card = *it;
        point_to_cards[card.point].insert(card);
    }

    PointSet tow_card_set;

    for (PointToCardMap::iterator it = point_to_cards.begin(); 
        it != point_to_cards.end(); 
        ++it) {
        set<TPokerCard>& card_set = it->second;

        if (card_set.size() >= 2) {
            tow_card_set.insert(it->first);
        }
    }
    if (tow_card_set.size() < 2) {
        return false;
    }

    while (match_cards.size() < 4 && !tow_card_set.empty()) {
        PointSet::iterator point_iter = tow_card_set.begin();
        int point = *point_iter;

        set<TPokerCard>& card_set = point_to_cards[point];
        while (card_set.size() >= 2 && match_cards.size() < 4) {
            for (int i = 0; i < 2; ++i) {
                set<TPokerCard>::iterator card_iter = card_set.begin();
                match_cards.push_back(*card_iter);
                card_set.erase(card_iter);                
            }
        }
        if (card_set.empty()) {
            point_to_cards.erase(point);
        }
        tow_card_set.erase(point_iter);
    }

    PointToCardMap::iterator iter = point_to_cards.begin();
    if (iter != point_to_cards.end()) {
        set<TPokerCard>& card_set = iter->second;
        assert(!card_set.empty());
        match_cards.push_back(*card_set.begin());
    }

    return true;
}


