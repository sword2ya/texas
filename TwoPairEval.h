#ifndef _TwoPairEval_H
#define _TwoPairEval_H

#include "IPokerTypeEval.h"

class CTwoPairEval : public IPokerTypeEval
{
public:
    ~CTwoPairEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);
};


#endif