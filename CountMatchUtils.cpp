#include "CountMatchUtils.h"

bool CCountMatchUtil::isMatch(const vector<TPokerCard>& cards, size_t count ,vector<TPokerCard>& match_cards)
{
    if (cards.size() < count ) { 
        return false;
    }
    typedef map<int, set<TPokerCard>, std::greater<int> > PointToCardMap;
    PointToCardMap point_to_cards;

    for (vector<TPokerCard>::const_iterator it = cards.begin(); it != cards.end(); ++it) {
        const TPokerCard& card = *it;
        point_to_cards[card.point].insert(card);
    }

    bool matched = false;
    for (PointToCardMap::iterator it = point_to_cards.begin(); 
        it != point_to_cards.end(); 
        ++it) {
        set<TPokerCard>& card_set = it->second;

        if (card_set.size() < count) {
            continue;
        }
        while (match_cards.size() < count && !card_set.empty()) {
            set<TPokerCard>::iterator card_iter = card_set.begin();
            match_cards.push_back(*card_iter);
            card_set.erase(card_iter);
        }

        if (card_set.empty()) {
            point_to_cards.erase(it);
        }
        matched = true;
        break;
    }

    if (!matched) {
        return false;
    }

    while (match_cards.size() < 5 && !point_to_cards.empty()) {
        PointToCardMap::iterator it = point_to_cards.begin();
        set<TPokerCard>& card_set = it->second;

        while (!card_set.empty() && match_cards.size() < 5) {
            set<TPokerCard>::iterator card_iter = card_set.begin();
            match_cards.push_back(*card_iter);
            card_set.erase(card_iter);
        }
        if (card_set.empty()) {
            point_to_cards.erase(it);
        }
    }

    return true;
}