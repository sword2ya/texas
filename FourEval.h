#ifndef _FourEval_H
#define _FourEval_H

#include "IPokerTypeEval.h"

// todo: 4条，3条，一对。可以写在一个算法中

class CFourEval : public IPokerTypeEval
{
public:
    ~CFourEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);
};


#endif