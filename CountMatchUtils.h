#ifndef _CountMatchUtil_H
#define _CountMatchUtil_H

#include "PokerEvalDef.h"

class CCountMatchUtil 
{
public:
    static bool isMatch(const vector<TPokerCard>& cards, size_t count ,vector<TPokerCard>& match_cards);
};

#endif 