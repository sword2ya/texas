#include "HighCardEval.h"
#include "CountMatchUtils.h"

CHighCardEval::~CHighCardEval()
{
}


bool CHighCardEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    typedef multimap<int, TPokerCard, std::greater<int> > CardMap;
    CardMap card_map;

    for (vector<TPokerCard>::const_iterator it = cards.begin(); it != cards.end(); ++it) {
        const TPokerCard& card = *it;
        card_map.insert(std::make_pair(card.point, card));
    }

    while (match_cards.size() < 5 && !card_map.empty()) {
        CardMap::iterator card_iter = card_map.begin();
        match_cards.push_back(card_iter->second);
        card_map.erase(card_iter);
    }
    return true;
}
