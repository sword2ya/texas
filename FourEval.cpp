#include "FourEval.h"
#include "CountMatchUtils.h"

CFourEval::~CFourEval()
{
}


bool CFourEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    return CCountMatchUtil::isMatch(cards, 4, match_cards);
}
