#ifndef _StraightEval_H
#define _StraightEval_H

#include "IPokerTypeEval.h"

class CStraightEval : public IPokerTypeEval
{
    typedef map<int, set<TPokerCard> > Point2CardsMap;
public:
    ~CStraightEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);

private:
    void CollectMatchCards(int point, const Point2CardsMap& point_to_cards, vector<TPokerCard>& match_cards);
};

#endif