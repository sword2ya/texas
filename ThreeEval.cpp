#include "ThreeEval.h"
#include "CountMatchUtils.h"

CThreeEval::~CThreeEval()
{
}


bool CThreeEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    return CCountMatchUtil::isMatch(cards, 3, match_cards);
}
