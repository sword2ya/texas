#ifndef _FullHourseEval_H
#define _FullHourseEval_H

#include "IPokerTypeEval.h"

class CFullHourseEval : public IPokerTypeEval
{
public:
    ~CFullHourseEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);
};


#endif