#ifndef _StraightFlushEval_H
#define _StraightFlushEval_H

#include "IPokerTypeEval.h"

class CStraightFlushEval : public IPokerTypeEval 
{
public:
    virtual ~CStraightFlushEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);
};

#endif