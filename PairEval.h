#ifndef _PairEval_H
#define _PairEval_H

#include "IPokerTypeEval.h"

class CPairEval : public IPokerTypeEval
{
public:
    ~CPairEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);
};


#endif