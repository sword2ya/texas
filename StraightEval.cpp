#include "StraightEval.h"

CStraightEval::~CStraightEval()
{
}


bool CStraightEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    if (cards.size() < 5) {
        return false;
    }
    Point2CardsMap point_to_cards;

    for (vector<TPokerCard>::const_iterator it = cards.begin(); it != cards.end(); ++it) {
        const TPokerCard& card = *it;
        point_to_cards[card.point].insert(card);
    }

    int point = eCardPoint_A + 1;
    for (int i = eCardPoint_A; i >= eCardPoint_2; --i) {
        set<TPokerCard> cards = point_to_cards[i];
        if (cards.size() == 0) {
            point = i;
            continue;
        }
        if (point - i >= 5) {
            CollectMatchCards(point - 1, point_to_cards, match_cards);
            return true;
        }
    }
    // A,2,3,4,5 
    if (point == eCardPoint_6 && point_to_cards[eCardPoint_A].size() != 0) {
        CollectMatchCards(eCardPoint_5, point_to_cards, match_cards);
        return true;
    }

    return false;
}

void CStraightEval::CollectMatchCards(int point, const Point2CardsMap& point_to_cards, vector<TPokerCard>& match_cards)
{
    assert(point >= eCardPoint_5);

    for (int i = 0; i < 5; ++i) {
        int cur_point = point - i;
        if (cur_point == eCardPoint_2 - 1) {
            cur_point = eCardPoint_A;
        }
        Point2CardsMap::const_iterator it = point_to_cards.find(cur_point);
        assert(it != point_to_cards.end());
        const set<TPokerCard>& cards = it->second;
        assert(!cards.empty());
        match_cards.push_back(*(cards.begin()));
    }
}