#ifndef _HighCardEval_H
#define _HighCardEval_H

#include "IPokerTypeEval.h"

class CHighCardEval : public IPokerTypeEval
{
public:
    ~CHighCardEval();
    virtual bool isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards);
};


#endif