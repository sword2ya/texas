#include "PokerEval.h"
#include "RoyalTypeEval.h"
#include "StraightFlushEval.h"
#include "FourEval.h"
#include "FullHourseEval.h"
#include "FlushEval.h"
#include "StraightEval.h"
#include "ThreeEval.h"
#include "TwoPairEval.h"
#include "PairEval.h"
#include "HighCardEval.h"

CPokerEval::CPokerEval() 
{
    _type_evaler_map[ePokerType_Royal] = new CRoyalTypeEval;
    _type_evaler_map[ePokerType_StraightFlush] = new CStraightFlushEval;
    _type_evaler_map[ePokerType_Four] = new CFourEval;
    _type_evaler_map[ePokerType_FullHouse] = new CFullHourseEval;
    _type_evaler_map[ePokerType_Flush] = new CFlushEval;
    _type_evaler_map[ePokerType_Straight] = new CStraightEval;
    _type_evaler_map[ePokerType_Three] = new CThreeEval;
    _type_evaler_map[ePokerType_TwoPair] = new CTwoPairEval;
    _type_evaler_map[ePokerType_Pair] = new CPairEval;
    _type_evaler_map[ePokerType_HighCard] = new CHighCardEval;
}

CPokerEval::~CPokerEval()
{
    for (PokerEvalMap::iterator it = _type_evaler_map.begin(); 
        it != _type_evaler_map.end(); 
        ++it) {
        delete it->second;
    }
}

TPokerValue CPokerEval::calcPokerType(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    TPokerValue poker_value;
    poker_value.poker_type = ePokerType_HighCard;
    poker_value.type_value = 0;

    for (PokerEvalMap::iterator it = _type_evaler_map.begin(); 
        it != _type_evaler_map.end();
        ++it) {
        IPokerTypeEval* evaler = it->second;
        if (evaler->isMatch(cards, match_cards)) {
            poker_value.poker_type = it->first;
            poker_value.type_value = calcTypeValue(match_cards);
            return poker_value;
        }
    }
    match_cards.clear();
    return poker_value;
}

int CPokerEval::calcTypeValue(const vector<TPokerCard>& cards)
{
    int value = 0;
    int index = 5;
    for (vector<TPokerCard>::const_iterator it = cards.begin(); 
        it != cards.end() && index > 0; ++it, --index) {
        const TPokerCard& card = *it;
        value += card.point * (index+15);   // +15的意思是要比一张牌的影响大
    }
    return value;
}