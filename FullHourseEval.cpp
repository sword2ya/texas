#include "FullHourseEval.h"

#include <iostream>

CFullHourseEval::~CFullHourseEval()
{
}


bool CFullHourseEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    if (cards.size() < 5) { 
        return false;
    }
    typedef map<int, set<TPokerCard>, std::greater<int> > PointToCardMap;
    PointToCardMap point_to_cards;

    for (vector<TPokerCard>::const_iterator it = cards.begin(); it != cards.end(); ++it) {
        const TPokerCard& card = *it;
        point_to_cards[card.point].insert(card);
    }

    PointToCardMap three_card_map;
    PointToCardMap two_card_map;

    for (PointToCardMap::iterator it = point_to_cards.begin(); 
        it != point_to_cards.end(); 
        ++it) {
        set<TPokerCard>& card_set = it->second;

        if (card_set.size() >= 3) {
            three_card_map[it->first] = card_set;
        }
        else if (card_set.size() == 2) {
            two_card_map[it->first] = card_set;
        }
    }

    if (three_card_map.empty() ||
        three_card_map.size() + two_card_map.size() < 2) {
        return false;
    }

    PointToCardMap::iterator iter = three_card_map.begin();
    set<TPokerCard> three_card_set = iter->second;

    for (set<TPokerCard>::iterator it = three_card_set.begin();
        it != three_card_set.end() && match_cards.size() < 3;
        ++it) {
        match_cards.push_back(*it);
    }
    three_card_map.erase(iter);

    set<TPokerCard> two_card_set; 
    PointToCardMap::iterator two_card_iter = two_card_map.begin();

    if (three_card_map.size() != 0) {
        PointToCardMap::iterator tmp_iter = three_card_map.begin();
        if (two_card_iter == two_card_map.end() || tmp_iter->first > two_card_iter->first) {
            two_card_iter = three_card_map.begin();
        }
    }
    two_card_set = two_card_iter->second;

    for (set<TPokerCard>::iterator it = two_card_set.begin(); 
        it != two_card_set.end() && match_cards.size() < 5; ++it ) {
        match_cards.push_back(*it);
    }
    return true;
}


