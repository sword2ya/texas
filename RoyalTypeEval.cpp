#include "RoyalTypeEval.h"
#include "StraightFlushEval.h"

CRoyalTypeEval::~CRoyalTypeEval()
{}

bool CRoyalTypeEval::isMatch(const vector<TPokerCard>& cards, vector<TPokerCard>& match_cards)
{
    CStraightFlushEval straight_flush_eval;

    if (!straight_flush_eval.isMatch(cards, match_cards)) {
        return false;
    }
    assert(!match_cards.empty());
    if (match_cards[0].point != eCardPoint_A) {
        match_cards.clear();
        return false;
    }
    return true;
}